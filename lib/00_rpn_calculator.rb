class RPNCalculator
  # to do: your code goes here!

  def initialize(value = nil, operands = [])
    @value = value
    @operands = operands
  end

  def value
    @value
  end

  def push(value)
    @operands.push(value)
  end

  def check_args
    if @operands.empty?
      raise "calculator is empty"
    end
  end

  def tokens(str)
    tokenized = []
    operators = '+-*/'

    str.split(' ').each do |ch|
      if operators.include?(ch)
        tokenized.push(ch.to_sym)
      else
        tokenized.push(ch.to_i)
      end
    end

    tokenized
  end

  def evaluate(str)
    @value = nil
    @operands = []
    tokenized = tokens(str)

    tokenized.each do |ch|
      if ch.is_a? Integer
        @operands.push(ch)
      elsif ch == :+
        self.plus
      elsif ch == :-
        self.minus
      elsif ch == :/
        self.divide
      elsif ch == :*
        self.times
      end
    end

    self.value
  end

  def plus
    self.check_args
    if @operands.length >= 2
      if @value.nil? == false
        @operands.push((@operands[-2] + @operands[-1]))
        @operands.shift(2)
      else
        @value = (@operands[-2] + @operands[-1])
        @operands.pop(2)
      end
    elsif @operands.length < 2
      @value = @value + @operands[0]
      @operands.pop
    end
  end

  def minus
    self.check_args
    if @operands.length >= 2
      if @value.nil? == false
        @operands.push((@operands[-2] - @operands[-1]))
        @operands.shift(2)
      else
        @value = (@operands[-2] - @operands[-1])
        @operands.pop(2)
      end
    elsif @operands.length < 2
      @value = @value - @operands[0]
      @operands.pop
    end
  end

  def divide
    self.check_args
    if @operands.length >= 2
      if @value.nil? == false
        @operands.push((@operands[-2].to_f / @operands[-1].to_f))
        @operands.shift(2)
      else
        @value = (@operands[-2].to_f / @operands[-1].to_f)
        @operands.pop(2)
      end
    elsif @operands.length < 2
      @value = @value.to_f / @operands[0].to_f
      @operands.pop
    end
  end

  def times
    self.check_args
    if @operands.length >= 2
      if @value.nil? == false
        @operands.push((@operands[-2] * @operands[-1]))
        @operands.shift(2)
      else
        @value = (@operands[-2] * @operands[-1])
        @operands.pop(2)
      end
    elsif @operands.length < 2
      @value = @value.to_f * @operands[0].to_f
      @operands.pop
    end
  end

end
